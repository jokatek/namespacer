<?php

use Namespacer\Controller\Controller;

return array(
    'Namespacer' => array(
        'disableUsage' => false,    // set to true to disable showing available ZFTool commands in Console.
    ),

    'controllers' => array(
        'invokables' => array(
            Controller::class => Controller::class,
        ),
    ),

    'console' => array(
        'router' => array(
            'routes' => array(
                'namespacer-create-map' => array(
                    'options' => array(
                        'route'    => 'map [--mapfile=] [--source=] [--root=]',
                        'defaults' => array(
                            'controller' => Controller::class,
                            'action'     => 'createMap',
                        ),
                    ),
                ),
                'namespacer-transform'  => array(
                    'options' => array(
                        'route'    => 'transform [--mapfile=] [--step=]',
                        'defaults' => array(
                            'controller' => Controller::class,
                            'action'     => 'transform',
                        ),
                    ),
                ),
                'namespacer-fix'        => array(
                    'options' => array(
                        'route'    => 'fix [--mapfile=] [--target=]',
                        'defaults' => array(
                            'controller' => Controller::class,
                            'action'     => 'fix',
                        ),
                    ),
                ),
            ),
        ),
    ),

);
